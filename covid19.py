'''
                                  ESP Health
                         Notifiable Diseases Framework
                            COVID-19 Case Generator


@author: Bob Zambarano <bzambarano@commoninf.com>
@organization: Commonwealth Informatics.
@contact: http://esphealth.org
@copyright: (c) 2020 Commonwealth Informatics
@license: LGPL
'''
import datetime
from django.db.models import Count, Q
from ESP.utils import log
from ESP.utils.utils import queryset_iterator
from ESP.hef.base import Event, BaseEventHeuristic, LabResultAnyHeuristic, LabOrderHeuristic
from ESP.hef.base import LabResultPositiveHeuristic, Dx_CodeQuery, HEF_CORE_URI
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import Case
from ESP.conf.models import ConditionConfig, LabTestMap
from ESP.static.models import DrugSynonym
from ESP.emr.models import Encounter, LabOrder
from django.contrib.contenttypes.models import ContentType
from dateutil.relativedelta import relativedelta



FEVER=100

class Covid19LabOrderHeuristic(LabOrderHeuristic):
    '''
    extension of LabOrderHeuristic to use procedure_name OR procedure_code 
    '''
    def __init__(self,test_name):
        super(Covid19LabOrderHeuristic,self).__init__(test_name)

    def __hash__(self):
        return hash(self.test_name)
    
    @property
    def _lab_test_maps(self):
        testmaps = LabTestMap.objects.filter(test_name=self.test_name)
        if not testmaps:
            log.warning('No tests mapped for "%s". Cannot generate events' % self.test_name)
        testmaps = testmaps.filter( Q(record_type='result') | Q(record_type='both'))
        return testmaps

    def generate(self):
        log.info('Generating events for %s' % self)
        f1 = Q(procedure_code__in=self._lab_test_maps.values('native_code'))
        f2 = Q(procedure_name__in=self._lab_test_maps.values('native_code'))
        alt = LabOrder.objects.filter(f1 | f2)
        unbound_orders = alt.exclude(events__name=self.order_event_name)
        counter = 0
        for order in queryset_iterator(unbound_orders):
            Event.create(
                name = self.order_event_name,
                source = self.uri,
                date = order.date,
                patient = order.patient,
                provider = order.provider,
                emr_record = order,
                )
            counter += 1
        log.info('Generated %s new %s events' % (counter, self))
        return counter


class FeverDiagnosisHeuristic(BaseEventHeuristic):
    '''
    A heuristic for detecting events based on fever diagnosis codes
    from a physician encounter, ensuring that the encounter temperature value is null
    '''
    def __init__(self, name, dx_code_queries):
        '''
        @param name: Name of this heuristic
        @type name:  String
        @param dx_code_queries: Generate event for records matching one of these queries
        @type dx_code_queries:  List of dx codes Query objects
        '''
        assert name and dx_code_queries
        self.name = name
        self.dx_code_queries = dx_code_queries

    def __hash__(self):
        return hash(self.name)

    @property
    def short_name(self):
        sn = u'diagnosis:%s' % self.name
        return sn
    
    uri = u'urn:x-esphealth:heuristic:channing:diagnosis:v1'
    
    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]
    
    def __str__(self):
        return self.short_name
    
    @property
    def encounters(self):
        
        enc_q = self.dx_code_queries[0].encounter_q_obj
        for query in self.dx_code_queries[1:]:
            enc_q |= query.encounter_q_obj
        return Encounter.objects.filter(enc_q)
    
    @property
    def dx_event_name(self):
        return 'dx:%s' % self.name
    
    @property
    def event_names(self):
        return [self.dx_event_name]
    
    def generate(self):
        #get the set of encounter PKs with fever diagnosis
        enc_qs_set = set(self.encounters.values_list('pk',flat=True))
        #get the set of encounter PKs attached to dx:fever events
        excl_qs_set = set(Event.objects.filter(name__in=self.event_names).values_list('object_id',flat=True))
        #get the list of encounter pk and temp values for the fever diagnosis encounters that are not events (yet)
        enc_pks_temp_tups = Encounter.objects.filter(pk__in=enc_qs_set - excl_qs_set).values_list('pk','temperature')
        #go through the list of tuples and if there is no temperature, then keep the pk.
        evnt_pks = set()
        for pk_tup in enc_pks_temp_tups:
             if not pk_tup[1]:
                 evnt_pks.add(pk_tup[0])
        #now get the set of encounter events for those pks, which should be new events.
        enc_qs = Encounter.objects.filter(pk__in=evnt_pks).order_by('date')
        log.info('Generating events for "%s"' % self)
        #log_query('Encounters for %s' % self, enc_qs)
        counter = 0
        for enc in queryset_iterator(enc_qs):
            Event.create(
                name = self.dx_event_name,
                source = self.uri,
                patient = enc.patient,
                date = enc.date,
                provider = enc.provider,
                emr_record = enc,
                )
            counter += 1
        log.info('Generated %s new events for %s' % (counter, self))
        return counter

class EncounterFeverHeuristic(BaseEventHeuristic):
    '''
    Fever Event Heuristic
    '''

    def __init__(self, name):
        '''
        @param name: Name of this heuristic
        @type name:  String
        '''
        assert name
        self.name = name

    def __hash__(self):
        return hash(self.name)

    @property
    def short_name(self):
        return u'enc:%s' % self.name

    uri = u'urn:x-esphealth:heuristic:channing:encbp:v1'
    
    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]

    @property
    def encounters(self):
        fev_enc_pk_lst = Encounter.objects.filter(temperature__gte = FEVER).values_list('pk',flat=True)
        fev_evnt_objid_lst = Event.objects.filter(name='enc:fever').values_list('object_id',flat=True)
        pk_lst = list(set(fev_enc_pk_lst)-set(fev_evnt_objid_lst))
        return Encounter.objects.filter(pk__in=pk_lst)

    @property
    def enc_event_name(self):
        return 'enc:%s' % self.name 

    @property
    def event_names(self):
        return [self.enc_event_name]

    def generate(self):
        log.info('Generating events for "%s"' % self)
        counter = 0
        for enc in queryset_iterator(self.encounters):
            Event.create(name = self.enc_event_name,
                    source=self.uri,
                    patient = enc.patient,
                    date = enc.date,
                    provider = enc.provider,
                    emr_record = enc,
                    )
            counter += 1

        log.info('Generated %s new events for %s' % (counter, self))
        return counter


class DiagnosisHeuristic(BaseEventHeuristic):
    '''
    A heuristic for detecting events based on one or more diagnosis codes
    from a physician encounter.
    '''
    def __init__(self, name, dx_code_queries):
        '''
        @param name: Name of this heuristic
        @type name:  String
        @param dx_code_queries: Generate event for records matching one of these queries
        @type dx_code_queries:  List of dx codes Query objects
        '''
        assert name and dx_code_queries
        self.name = name
        self.dx_code_queries = dx_code_queries

    def __hash__(self):
        return hash(self.name)

    @property
    def short_name(self):
        sn = u'diagnosis:%s' % self.name
        return sn
    
    uri = u'urn:x-esphealth:heuristic:channing:diagnosis:v1'
    
    # Only this version of HEF is supported
    core_uris = [HEF_CORE_URI]
    
    def __str__(self):
        return self.short_name
    
    @property
    def encounters(self):
        #local mod to just return set of enc pks
        enc_q = self.dx_code_queries[0].encounter_q_obj
        for query in self.dx_code_queries[1:]:
            enc_q |= query.encounter_q_obj
        return set(Encounter.objects.filter(enc_q).values_list('pk',flat=True))
    
    @property
    def dx_event_name(self):
        return 'dx:%s' % self.name
    
    @property
    def event_names(self):
        return [self.dx_event_name]
    
    def generate(self):
        enc_pkset = self.encounters
        pk_lst = list(enc_pkset - set(Event.objects.filter(name__in=self.event_names).values_list('object_id',flat=True)))
        enc_qs = Encounter.objects.filter(pk__in=pk_lst).values('pk','patient','date','provider')
        log.info('Generating events for "%s"' % self)
        #log_query('Encounters for %s' % self, enc_qs)
        counter = 0
        content_type = ContentType.objects.get_for_model(Encounter)
        for enc in enc_qs:
            e = Event(
                name = self.dx_event_name,
                source = self.uri,
                date = enc['date'],
                patient_id = enc['patient'],
                provider_id = enc['provider'],
                content_type = content_type,
                object_id = enc['pk'],
                )
            e.save()
            counter += 1
        log.info('Generated %s new events for %s' % (counter, self))
        return counter

class covid19(DiseaseDefinition):
    '''
    covid-19 case definition
    '''
    
    conditions = ['covid19']

    uri = 'urn:x-esphealth:disease:commoninf:covid19:v1'
    
    short_name = 'covid19'
    
    test_name_search_strings = [ 'covid', 'sars', 'cov', 'corona' ]
    
    timespan_heuristics = []
    
    recurrence_interval = 42
    
    status='AR'
    
    @property
    def event_heuristics(self):
        heuristic_list = []
        
        #
        # Diagnosis Codes for covid-19
        #        
        heuristic_list.append( DiagnosisHeuristic(
            name = 'covid19',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='B34.2', type='icd10'),
                 Dx_CodeQuery(starts_with='B97.29', type='icd10'),
                 Dx_CodeQuery(starts_with='U07.1', type='icd10'),
                ]))
                
        #
        # Diagnosis Codes for Pneumonia
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'pneumonia',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='J12.89', type='icd10'),
                 Dx_CodeQuery(starts_with='J12.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J17', type='icd10'),
                 Dx_CodeQuery(starts_with='J18.0', type='icd10'),
                 Dx_CodeQuery(starts_with='J18.1', type='icd10'),
                 Dx_CodeQuery(starts_with='J18.8', type='icd10'),
                 Dx_CodeQuery(starts_with='J18.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J12.82', type='icd10'),
                ]))
        
        #
        # Diagnosis Codes for bronchitis
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'bronchitis',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='J20.8', type='icd10'),
                 Dx_CodeQuery(starts_with='J20.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J21.8', type='icd10'),
                 Dx_CodeQuery(starts_with='J21.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J40', type='icd10'),
                 ]))

        #
        # Diagnosis Codes for lower respiratory infection (lri)
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'lri',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='J22', type='icd10'),
                 Dx_CodeQuery(starts_with='J98.8', type='icd10'),
                 ]))

        #
        # Diagnosis Codes for acute respiratory distress syndrome (ards)
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'ards',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='J80', type='icd10'),
                 ]))

        #
        #
        heuristic_list.append( EncounterFeverHeuristic(
            name = 'fever'
            ))        

        #
        # Diagnosis Codes for fever
        #  
        heuristic_list.append( DiagnosisHeuristic(
            name = 'fever',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='R50.2', type='icd10'),
                 Dx_CodeQuery(starts_with='R50.8', type='icd10'),
                 Dx_CodeQuery(starts_with='R50.84', type='icd10'),
                 Dx_CodeQuery(starts_with='R50.9', type='icd10'),
                 Dx_CodeQuery(starts_with='R56.00',type='icd10'),
                ]))

        #
        # Diagnosis Codes for viral infections
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'viralinfection',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='B33.8', type='icd10'),
                 Dx_CodeQuery(starts_with='B34.8', type='icd10'),
                 Dx_CodeQuery(starts_with='B97.89', type='icd10'),
                 ]))
                 
        #
        # Diagnosis Codes for upper respiratory infections
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'urespinfection',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='J00', type='icd10'),
                 Dx_CodeQuery(starts_with='J02.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J04.0', type='icd10'),
                 Dx_CodeQuery(starts_with='J04.10', type='icd10'),
                 Dx_CodeQuery(starts_with='J04.11', type='icd10'),
                 Dx_CodeQuery(starts_with='J04.2', type='icd10'),
                 Dx_CodeQuery(starts_with='J05.0', type='icd10'),
                 Dx_CodeQuery(starts_with='J06.0', type='icd10'),
                 Dx_CodeQuery(starts_with='J06.9', type='icd10'),
                 Dx_CodeQuery(starts_with='J39.8', type='icd10'),
                 Dx_CodeQuery(starts_with='J39.9', type='icd10'),
                 ]))

        #
        # Diagnosis Codes for cough
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'cough',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='R07.0', type='icd10'),
                 Dx_CodeQuery(starts_with='R05', type='icd10'),
                 ]))

        #
        # Diagnosis Codes for shortness of breath
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'shortofbreath',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='R06.02', type='icd10'),
                 ]))

        #
        # Diagnosis Codes for olfactory and taste disorder(s)
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'olfactory-taste-disorders',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='R43.0', type='icd10'),
                 Dx_CodeQuery(starts_with='R43.1', type='icd10'),
                 Dx_CodeQuery(starts_with='R43.2', type='icd10'),
                 Dx_CodeQuery(starts_with='R43.8', type='icd10'),
                 Dx_CodeQuery(starts_with='R43.9', type='icd10'),
                 ]))
        #
        # Diagnosis Codes for multisystem inflammatory syndrome 
        #
        heuristic_list.append( DiagnosisHeuristic(
            name = 'multi-inflam-syndrome',
            dx_code_queries = [
                 Dx_CodeQuery(starts_with='M35.81', type='icd10'),
                 ]))

        #
        # LabOrders for Chest CT Scan
        #
        heuristic_list.append( Covid19LabOrderHeuristic(
            test_name = 'chestct',
             ))

        #
        # LabOrders for Chest xray Scan
        #
        heuristic_list.append( Covid19LabOrderHeuristic(
            test_name = 'chestxray',
             ))
             
        #
        # LabResults for COVID-19
        #
        heuristic_list.append( LabResultPositiveHeuristic(
            test_name = 'covid19_pcr',
             ))

        #
        # LabOrders for COVID-19
        #
        heuristic_list.append( Covid19LabOrderHeuristic(
            test_name = 'covid19_pcr_order',
             ))

        return heuristic_list

    def add_events_to_existing_cases(self,patients,events): 
        #I'm createing this locally because the nodis/base version takes events and creates a full event dict, which uses more memory
        case_patients = set()
        existing_cases = Case.objects.filter(
            patient__in = patients,
            condition = self.conditions[0],
            )
        for case in existing_cases:
            case_patients.add(case.patient)
            self._update_case_from_event_list(case,
                            relevant_events = events.get(case.patient.id, []))
        return case_patients
    
    def create_case_from_event_qs(self, 
            condition,
            criteria,
            recurrence_interval,
            event_qs, 
            relevant_event_names = [],):
                   
        counter = 0
        for this_event in event_qs:
            created, this_case = self._create_case_from_event_obj(
                condition = condition, 
                criteria = criteria, 
                recurrence_interval = recurrence_interval, 
                event_obj = this_event, 
                relevant_event_names = relevant_event_names,
                )
            if created:
                counter += 1
        return counter
        
    def _make_pat_dict(self,values_qset):
        #takes a queryset with values [form:qs.values('pk','patient','date')] containing patient id, model pk and date
        # will also use provider if present
        # returns dictionary of patient keys, each with a list of tuples containing model pk and date (and provider if present)
        pat_tups_dict = {}
        for rec in values_qset:
            patkey=rec['patient']
            pkval=rec['pk']
            dateval=rec['date']
            try:
                provval=rec['provider']
                try:
                    pat_tups_dict[patkey].append((pkval,dateval,provval))
                except KeyError:
                    pat_tups_dict[patkey] = [(pkval,dateval,provval)]
            except KeyError:
                try:
                    pat_tups_dict[patkey].append((pkval,dateval))
                except KeyError:
                    pat_tups_dict[patkey] = [(pkval,dateval)]
        dict_pats = set(pat_tups_dict.keys())
        return pat_tups_dict, dict_pats
            
    def generate(self):
        log.info('Generating cases of %s' % self.short_name)       
        
        fever_ev_names = ['dx:fever', 'enc:fever']
        tab2_evset_names = ['dx:pneumonia','dx:bronchitis','dx:lri','dx:ards']
        # the following includes cat1 and cat2 events (not including fever or covid)
        tab2plus_evset_names = ['dx:pneumonia','dx:bronchitis','dx:lri','dx:ards','lx:chestxray:order','lx:chestct:order']
        # tab4 is category 3 not including fever
        tab4_evset_names = ['dx:viralinfection','dx:urespinfection','dx:cough','dx:shortofbreath']
        c19lx_evset_names =  ['lx:covid19_pcr:positive','lx:covid19_pcr:negative','lx:covid19_pcr_order:order','lx:covid19_pcr:indeterminate']

        cat4_evset_names = ['dx:pneumonia','dx:bronchitis','dx:lri','dx:ards']
        cat5_evset_names = ['dx:viralinfection','dx:urespinfection','dx:cough','dx:shortofbreath','dx:olfactory-taste-disorders'] 
        cat6_evset_names = ['dx:covid19','dx:pneumonia','dx:bronchitis','dx:lri','dx:ards','dx:viralinfection','dx:urespinfection','dx:cough','dx:shortofbreath','dx:olfactory-taste-disorders','dx:multi-inflam-syndrome','dx:fever', 'enc:fever']
        
        #events already attached to cases as python set
        case_evnt_pks = set(Case.objects.filter(condition=self.conditions[0]).values_list('events__pk',flat=True))

        #new events.  There were no confirmed US cases of covid-19 before 15 Jan, 2020.  Using 01 July 2017 per email from mklompas to bzambarano 23032020:00:34
        #I was filtering these for clin_enc encounters, but that is needless, as any encounter with a diagnosis code is by definition a clinical encounter.
        #In the first pass below, these events are just used for attaching to existing cases. 
        #NB: pcr tests are not used for detection but are attached to previously identified cases.
        dx_covid19_pk = set(Event.objects.filter(name__exact = 'dx:covid19', date__gte=datetime.datetime(2017,7,1)).values_list('pk',flat=True)) - case_evnt_pks
        fev_evnt_pk = set(Event.objects.filter(name__in = fever_ev_names, date__gte=datetime.datetime(2017,7,1)).values_list('pk',flat=True)) - case_evnt_pks
        tab2_evset_pk = set(Event.objects.filter(name__in = tab2_evset_names, date__gte=datetime.datetime(2017,7,1)).values_list('pk',flat=True)) - case_evnt_pks
        tab2plus_evset_pk = set(Event.objects.filter(name__in = tab2plus_evset_names, date__gte=datetime.datetime(2017,7,1)).values_list('pk',flat=True)) - case_evnt_pks
        tab4_evset_pk = set(Event.objects.filter(name__in = tab4_evset_names, date__gte=datetime.datetime(2017,7,1)).values_list('pk',flat=True)) - case_evnt_pks
        #the date on the next one is set to jan 1 2020 as there were no tests prior to this date
        c19lx_evset_pk = set(Event.objects.filter(name__in = c19lx_evset_names, date__gte=datetime.datetime(2020,1,1)).values_list('pk',flat=True)) - case_evnt_pks

        cat4_evset_pk = set(Event.objects.filter(name__in = cat4_evset_names, date__gte=datetime.datetime(2017,7,1)).values_list('pk',flat=True)) - case_evnt_pks
        cat5_evset_pk = set(Event.objects.filter(name__in = cat5_evset_names, date__gte=datetime.datetime(2017,7,1)).values_list('pk',flat=True)) - case_evnt_pks
        cat6_evset_pk = set(Event.objects.filter(name__in = cat6_evset_names, date__gte=datetime.datetime(2017,7,1)).values_list('pk',flat=True)) - case_evnt_pks

        all_event_pks = dx_covid19_pk | fev_evnt_pk | tab2plus_evset_pk | tab4_evset_pk | c19lx_evset_pk | cat4_evset_pk | cat5_evset_pk | cat6_evset_pk
        #Order the events by date
        all_event_qs = Event.objects.filter(pk__in=all_event_pks).values('patient','pk','date').order_by('patient', 'date')

        #build dictionaries of patients and events
        all_event_dict, event_pats = self._make_pat_dict(all_event_qs)


        #build a dictionary of patients and cases
        case_lists_qs = Case.objects.filter(condition=self.conditions[0]).values('patient','pk','date')
        case_lists_dict, case_pats = self._make_pat_dict(case_lists_qs)

        #now add any of these new events to existing cases.
        added_evnts = set()
        for pat in event_pats:
            #sort events by date
            pat_events = all_event_dict[pat]
            for event in pat_events:
                #check to see if event can be attached to an existing case:
                try:
                    case_list = case_lists_dict[pat]
                    if len(case_list) > 1:
                        case_list.sort(key=lambda x: x[1])
                    for case in case_list:
                        if abs((event[1]-case[1]).days) < self.recurrence_interval:
                            case_obj = Case.objects.get(pk=case[0])
                            case_obj.events.add(event[0])
                            case_obj.save()
                            #add the event to the case events set
                            added_evnts.add(event[0])
                            break
                except KeyError:
                    #no cases for this patient, so pass
                    pass

        #now pull the remaining events based on their PKs.  
        #this excludes any events previously or just attached to a case
        #covid
        dx_covid19_pk_reduced = dx_covid19_pk - added_evnts
        dx_covid19_qs = Event.objects.filter(pk__in = dx_covid19_pk_reduced).values('pk','patient','date','provider').order_by('date')    
        covid19_dict, covid19_pats = self._make_pat_dict(dx_covid19_qs)
        #fever
        fev_evnt_reduced = fev_evnt_pk - added_evnts
        fev_evnt_qs = Event.objects.filter(pk__in = fev_evnt_reduced).values('pk','patient','date','provider').order_by('date')
        fev_dict, fev_pats = self._make_pat_dict(fev_evnt_qs)
        #tab2
        tab2_evset_reduced = tab2_evset_pk - added_evnts
        tab2_evset_qs = Event.objects.filter(pk__in = tab2_evset_reduced).values('pk','patient','date','provider').order_by('date') 
        tab2_dict, tab2_pats = self._make_pat_dict(tab2_evset_qs)
        #tab2 plus image orders
        tab2plus_evset_reduced = tab2plus_evset_pk - added_evnts
        tab2plus_evset_qs = Event.objects.filter(pk__in = tab2plus_evset_reduced).values('pk','patient','date','provider').order_by('date')
        tab2plus_dict, tab2plus_pats = self._make_pat_dict(tab2plus_evset_qs)
        #tab4
        tab4_evset_reduced = tab4_evset_pk - added_evnts
        tab4_evset_qs = Event.objects.filter(pk__in = tab4_evset_reduced).values('pk','patient','date','provider').order_by('date')
        tab4_dict, tab4_pats = self._make_pat_dict(tab4_evset_qs)

        #cat4
        cat4_evset_reduced = cat4_evset_pk - added_evnts
        cat4_evset_qs = Event.objects.filter(pk__in = cat4_evset_reduced).values('pk','patient','date','provider').order_by('date')
        cat4_dict, cat4_pats = self._make_pat_dict(cat4_evset_qs)

        #cat5
        cat5_evset_reduced = cat5_evset_pk - added_evnts
        cat5_evset_qs = Event.objects.filter(pk__in = cat5_evset_reduced).values('pk','patient','date','provider').order_by('date')
        cat5_dict, cat5_pats = self._make_pat_dict(cat5_evset_qs)

        #cat6
        cat6_evset_reduced = cat6_evset_pk - added_evnts
        cat6_evset_qs = Event.objects.filter(pk__in = cat6_evset_reduced).values('pk','patient','date','provider').order_by('date')
        cat6_dict, cat6_pats = self._make_pat_dict(cat6_evset_qs)


        # Rebuild these dictionaries without the events that were just added to cases
        # Don't inlcude c19lx_evset_pk because they aren't part of case criteria. They would be attached above
        # Don't inlcude tab2iplus_evset_reduced as non lab orders will be captured by tab2_evset_reduced
        # Lab Orders only apply to cat1 and require a covid_dx code which has to be the trigger event
        all_event_pks_reduced = dx_covid19_pk_reduced | fev_evnt_reduced | tab2_evset_reduced | tab4_evset_reduced | cat4_evset_reduced | cat5_evset_reduced | cat6_evset_reduced
        #Order the events by date
        all_event_qs = Event.objects.filter(pk__in=all_event_pks_reduced).values('patient','pk','date').order_by('patient', 'date')

        #build dictionaries of patients and events
        all_event_dict, event_pats = self._make_pat_dict(all_event_qs)


        #time to make the cases
        #I have to make all the cat1 then all cat2 then all cat3 in order so that events create the correct category case.
        #cat 1 is patients with a covid19 diagnosis (dx_covid19 set) plus the extended table 2 set within 14 days
        counter = 0
        newcase_dict = {}
        for pat in covid19_pats:
            lastcasedt = None
            new_case = None
            c19list = covid19_dict[pat]
            if len(c19list) > 1:
               #sort the list by date
               c19list.sort(key=lambda x: x[1])
            for c19tup in c19list:

                # Get the first event (from all categories for the patient)
                all_events = all_event_dict[pat]
                if len(all_events) > 1:
                    #sort the list by date
                    all_events.sort(key=lambda x: x[1])
                earliest_event = all_events[0]

                # if the earliest event (of any type) and earliest covid event aren't within 14 days continue
                # as another category may be more applicable for the event
                if abs((earliest_event[1]-c19tup[1]).days) > 14:
                    continue
                elif lastcasedt and abs((c19tup[1]-lastcasedt).days) <= self.recurrence_interval:
                    new_case.events.add(c19tup[0])
                    new_case.save()
                else:
                    try:
                        remove_list = []
                        for tab2tup in tab2plus_dict[pat]:
                            if ((((lastcasedt and abs((tab2tup[1]-lastcasedt).days) > self.recurrence_interval) ) or not lastcasedt) 
                                and abs((c19tup[1]-tab2tup[1])).days <= 14):
                                case_date = min([c19tup[1],tab2tup[1]])
                                try:
                                    newcase_dict[pat].append(case_date)
                                except KeyError:
                                    newcase_dict[pat] = [case_date]
                                if tab2tup[1] == case_date:
                                    event = tab2tup
                                    othevent = c19tup
                                else:
                                    event = c19tup
                                    othevent = tab2tup
                                new_case = Case(patient_id=pat, 
                                                provider_id=event[2], 
                                                date=case_date, 
                                                condition=self.conditions[0],
                                                criteria= 'Category 1',
                                                source=self.uri,
                                                status=self.status )
                                new_case.save()
                                counter += 1
                                new_case.events.add(othevent[0])
                                new_case.events.add(event[0])
                                new_case.save()
                                lastcasedt=case_date
                                remove_list.append(tab2tup)
                            elif lastcasedt and abs((tab2tup[1]-lastcasedt).days) <= self.recurrence_interval:
                                #if there is a lastcasedt and the current event is within the recurrence_interval window, 
                                #  we can assume new_case was created and we can add more events to it.
                                new_case.events.add(tab2tup[0])
                                new_case.save()                               
                                remove_list.append(tab2tup)
                        #once attached to a case, I want to remove these.
                        tab2plus_dict[pat]=list(set(tab2plus_dict[pat])-set(remove_list))
                        tab2_dict[pat]=list(set(tab2_dict[pat])-set(remove_list))
                    except KeyError:
                        #we'd get here if there are not tab2 events for this patient, so just break out of the c19 loop for this patient
                        break

        #Now cat 2.
        #Cat 2 is patients with fever vitals or dx, plus any table 2 conditions within 14 days.
        for pat in fev_pats:
            lastcasedt = None
            new_case = None
            fevlist = fev_dict[pat]
            if len(fevlist) > 1:
               #sort the list by date
               fevlist.sort(key=lambda x: x[1])
            removef_list = []
            removet_list = []
            for fevtup in fevlist:
                all_events = all_event_dict[pat]
                if len(all_events) > 1:
                    #sort the list by date
                    all_events.sort(key=lambda x: x[1])
                earliest_event = all_events[0]
                # if the earliest event (of any type) and earliest fever event aren't within 14 days continue
                # as another category may be more applicable for the event
                if abs((earliest_event[1]-fevtup[1]).days) > 14:
                    continue
                #first check if the fever can be added to a case just created (we already added events to pre-existing cases)
                elif newcase_dict.get(pat,None):
                    casedate = [x for x in newcase_dict[pat] if abs(x - fevtup[1]).days <= self.recurrence_interval]
                    if len(casedate)>0:
                        #should only be one case within the recurrecnce interval
                        existingcase = Case.objects.get(patient__exact=pat,date__exact=casedate[0],condition__exact=self.conditions[0])
                        existingcase.events.add(fevtup[0])
                        existingcase.save
                        removef_list.append(fevtup)
                        #move on to the next fevtup in the list
                        continue
                if lastcasedt and abs((fevtup[1]-lastcasedt).days) <= self.recurrence_interval:
                    new_case.events.add(fevtup[0])
                    new_case.save()
                    removef_list.append(fevtup)
                else:
                    try:
                        for tab2tup in tab2_dict[pat]:
                            if newcase_dict.get(pat,None):
                                casedate = [x for x in newcase_dict[pat] if abs(x - tab2tup[1]).days <= self.recurrence_interval]
                                if len(casedate)>0:
                                    #should only be one case within the recurrecnce interval
                                    existingcase = Case.objects.get(patient__exact=pat,date__exact=casedate[0],condition__exact=self.conditions[0])
                                    existingcase.events.add(tab2tup[0])
                                    existingcase.save
                                    removet_list.append(tab2tup)
                                    #move on to the next tab2tup in the list
                                    continue
                            if ((((lastcasedt and abs((tab2tup[1]-lastcasedt).days) > self.recurrence_interval)) or not lastcasedt) 
                                and abs((fevtup[1]-tab2tup[1]).days) <= 14):
                                case_date = min([fevtup[1],tab2tup[1]])
                                try:
                                    newcase_dict[pat].append(case_date)
                                except KeyError:
                                    newcase_dict[pat] = [case_date]
                                if fevtup[1] == case_date:
                                    event = tab2tup
                                    othevent = fevtup
                                else:
                                    event = fevtup
                                    othevent = tab2tup
                                new_case = Case(patient_id=pat, 
                                                provider_id=event[2], 
                                                date=case_date, 
                                                condition=self.conditions[0],
                                                criteria= 'Category 2',
                                                source=self.uri,
                                                status=self.status )
                                new_case.save()
                                counter += 1
                                new_case.events.add(othevent[0])
                                new_case.events.add(event[0])
                                new_case.save()
                                lastcasedt=case_date
                                removef_list.append(fevtup)
                                removet_list.append(tab2tup)
                            elif lastcasedt and abs((tab2tup[1]-lastcasedt).days) <= self.recurrence_interval:
                                #if there is a lastcasedt and the current event is within the recurrence_interval window, 
                                #  we can assume new_case was created and we can add more events to it.
                                new_case.events.add(tab2tup[0])
                                new_case.save()                               
                                removef_list.append(fevtup)
                                removet_list.append(tab2tup)
                        tab2_dict[pat]=list(set(tab2_dict[pat])-set(removet_list))
                    except KeyError:
                        #we'd get here if there are not tab2 events for this patient, so just break out of the loop for this patient
                        break
            #once attached to a case, I want to remove these.
            fev_dict[pat]=list(set(fev_dict[pat])-set(removef_list))
        #Now cat 3.
        #Cat 3 is fever, plus any of the table 4 conditions within 14 days.
        for pat in fev_pats:
            lastcasedt = None
            new_case = None
            fevlist = fev_dict[pat]
            if len(fevlist) > 1:
               #sort the list by date
               fevlist.sort(key=lambda x: x[1])
            remove_list=[]
            for fevtup in fevlist:
                all_events = all_event_dict[pat]
                if len(all_events) > 1:
                    #sort the list by date
                    all_events.sort(key=lambda x: x[1])
                earliest_event = all_events[0]
                # if the earliest event (of any type) and earliest fever event aren't within 14 days continue
                # as another category may be more applicable for the event
                if abs((earliest_event[1]-fevtup[1]).days) > 14:
                    continue
                #first check if the fever can be added to a cat 1 or 2 case just created
                elif newcase_dict.get(pat,None):
                    casedate = [x for x in newcase_dict[pat] if abs(x - fevtup[1]).days <= self.recurrence_interval]
                    if len(casedate)>0:
                        #should only be one case within the recurrecnce interval
                        existingcase = Case.objects.get(patient__exact=pat,date__exact=casedate[0],condition__exact=self.conditions[0])
                        existingcase.events.add(fevtup[0])
                        existingcase.save
                        #move on to the next fevtup in the list
                        continue
                if lastcasedt and abs((fevtup[1]-lastcasedt).days) <= self.recurrence_interval:
                    new_case.events.add(fevtup[0])
                    new_case.save()
                else:
                    try:
                        for tab4tup in tab4_dict[pat]:
                            if newcase_dict.get(pat,None):
                                casedate = [x for x in newcase_dict[pat] if abs(x - tab4tup[1]).days <= self.recurrence_interval]
                                if len(casedate)>0:
                                    #should only be one case within the recurrecnce interval
                                    existingcase = Case.objects.get(patient__exact=pat,date__exact=casedate[0],condition__exact=self.conditions[0])
                                    existingcase.events.add(tab4tup[0])
                                    existingcase.save
                                    remove_list.append(tab4tup)
                                    #move on to the next tab4tup in the list
                                    continue
                            if ((((lastcasedt and abs((tab4tup[1]-lastcasedt).days) > self.recurrence_interval)) or not lastcasedt) 
                                and abs((fevtup[1]-tab4tup[1]).days) <= 14):
                                case_date = min([fevtup[1],tab4tup[1]])
                                try:
                                    newcase_dict[pat].append(case_date)
                                except KeyError:
                                    newcase_dict[pat] = [case_date]
                                if fevtup[1] == case_date:
                                    event = tab4tup
                                    othevent = fevtup
                                else:
                                    event = fevtup
                                    othevent = tab4tup
                                new_case = Case(patient_id=pat, 
                                                provider_id=event[2], 
                                                date=case_date, 
                                                condition=self.conditions[0],
                                                criteria= 'Category 3',
                                                source=self.uri,
                                                status=self.status )
                                new_case.save()
                                counter += 1
                                new_case.events.add(othevent[0])
                                new_case.events.add(event[0])
                                new_case.save()
                                lastcasedt=case_date
                                remove_list.append(tab4tup)
                            elif lastcasedt and abs((tab4tup[1]-lastcasedt).days) <= self.recurrence_interval:
                                #if there is a lastcasedt and the current event is within the recurrence_interval window, 
                                #  we can assume new_case was created and we can add more events to it.
                                new_case.events.add(tab4tup[0])
                                new_case.save()                               
                                remove_list.append(tab4tup)
                    except KeyError:
                        #we'd get here if there are not tab4 events for this patient, so just break out of the loop for this patient
                        break
                    tab4_dict[pat]=list(set(tab4_dict[pat])-set(remove_list))                
        
        #Now cat 4.
        #Cat 4 is patients with any table 2 condition
        for pat in cat4_pats:
            lastcasedt = None
            new_case = None
            remove_list=[]
            cat4list = cat4_dict[pat]
            if len(cat4list) > 1:
               #sort the list by date
               cat4list.sort(key=lambda x: x[1])
            try:
                for cat4tup in cat4list:
                    all_events = all_event_dict[pat]
                    if len(all_events) > 1:
                        #sort the list by date
                        all_events.sort(key=lambda x: x[1])
                    earliest_event = all_events[0]

                    if newcase_dict.get(pat,None):
                        casedate = [x for x in newcase_dict[pat] if abs(x - cat4tup[1]).days <= self.recurrence_interval]
                        if len(casedate)>0:
                            #should only be one case within the recurrecnce interval
                            existingcase = Case.objects.get(patient__exact=pat,date__exact=casedate[0],condition__exact=self.conditions[0])
                            existingcase.events.add(cat4tup[0])
                            existingcase.save
                            remove_list.append(cat4tup)
                            #move on to the next cat4tup in the list
                            continue

                    # if the earliest event (of any type) and earliest fever event aren't  
                    # on the same date, move to the next patient
                    # as another category may be more applicable for the event
                    if abs((earliest_event[1]-cat4tup[1]).days) > 0: 
                        break 

                    elif (((lastcasedt and abs((cat4tup[1]-lastcasedt).days) > self.recurrence_interval)) or not lastcasedt):
                        case_date = min([cat4tup[1]])
                        try:
                            newcase_dict[pat].append(case_date)
                        except KeyError:
                            newcase_dict[pat] = [case_date]
                        event = cat4tup
                        new_case = Case(patient_id=pat,
                                        provider_id=event[2],
                                        date=case_date,
                                        condition=self.conditions[0],
                                        criteria= 'Category 4',
                                        source=self.uri,
                                        status=self.status )
                        new_case.save()
                        counter += 1
                        new_case.events.add(event[0])
                        new_case.save()
                        lastcasedt=case_date
                        remove_list.append(cat4tup)
                    elif lastcasedt and abs((cat4tup[1]-lastcasedt).days) <= self.recurrence_interval:
                        #if there is a lastcasedt and the current event is within the recurrence_interval window,
                        #  we can assume new_case was created and we can add more events to it.
                        new_case.events.add(cat4tup[0])
                        new_case.save()
                        remove_list.append(cat4tup)
            except KeyError:
            #we'd get here if there are not cat4 events for this patient, so just break out of the loop for this patient
                break
            cat4_dict[pat]=list(set(cat4_dict[pat])-set(remove_list))

        #Now cat 5.
        #Cat 5 is patients with any table 4 condition
        for pat in cat5_pats:
            lastcasedt = None
            new_case = None
            remove_list=[]
            cat5list = cat5_dict[pat]
            if len(cat5list) > 1:
               #sort the list by date
               cat5list.sort(key=lambda x: x[1])
            try:
                for cat5tup in cat5list:
                    all_events = all_event_dict[pat]
                    if len(all_events) > 1:
                        #sort the list by date
                        all_events.sort(key=lambda x: x[1])
                    earliest_event = all_events[0]
                    
                    if newcase_dict.get(pat,None):
                        casedate = [x for x in newcase_dict[pat] if abs(x - cat5tup[1]).days <= self.recurrence_interval]
                        if len(casedate)>0:
                            #should only be one case within the recurrecnce interval
                            existingcase = Case.objects.get(patient__exact=pat,date__exact=casedate[0],condition__exact=self.conditions[0])
                            existingcase.events.add(cat5tup[0])
                            existingcase.save
                            remove_list.append(cat5tup)
                            #move on to the next cat5tup in the list
                            continue
                    # if the earliest event (of any type) and earliest fever event aren't        
                    # on the same date, move to the next patient
                    # as another category may be more applicable for the event
                    if abs((earliest_event[1]-cat5tup[1]).days) > 0:
                        break 
                    elif (((lastcasedt and abs((cat5tup[1]-lastcasedt).days) > self.recurrence_interval)) or not lastcasedt):
                        case_date = min([cat5tup[1]])
                        try:
                            newcase_dict[pat].append(case_date)
                        except KeyError:
                            newcase_dict[pat] = [case_date]
                        event = cat5tup
                        new_case = Case(patient_id=pat,
                                        provider_id=event[2],
                                        date=case_date,
                                        condition=self.conditions[0],
                                        criteria= 'Category 5',
                                        source=self.uri,
                                        status=self.status )
                        new_case.save()
                        counter += 1
                        new_case.events.add(event[0])
                        new_case.save()
                        lastcasedt=case_date
                        remove_list.append(cat5tup)
                    elif lastcasedt and abs((cat5tup[1]-lastcasedt).days) <= self.recurrence_interval:
                        #if there is a lastcasedt and the current event is within the recurrence_interval window,
                        #  we can assume new_case was created and we can add more events to it.
                        new_case.events.add(cat5tup[0])
                        new_case.save()
                        remove_list.append(cat5tup)
            except KeyError:
            #we'd get here if there are not cat5 events for this patient, so just break out of the loop for this patient
                break
            cat5_dict[pat]=list(set(cat5_dict[pat])-set(remove_list))

        #Now cat 6.
        #Cat 6 is patients with any condition (excluding chest ct)
        for pat in cat6_pats:
            lastcasedt = None
            new_case = None
            remove_list=[]
            cat6list = cat6_dict[pat]
            if len(cat6list) > 1:
               #sort the list by date
               cat6list.sort(key=lambda x: x[1])
            try:
                for cat6tup in cat6list:
                    all_events = all_event_dict[pat]
                    if len(all_events) > 1:
                        #sort the list by date
                        all_events.sort(key=lambda x: x[1])
                    earliest_event = all_events[0]

                    if newcase_dict.get(pat,None):
                        casedate = [x for x in newcase_dict[pat] if abs(x - cat6tup[1]).days <= self.recurrence_interval]
                        if len(casedate)>0:
                            #should only be one case within the recurrecnce interval
                            existingcase = Case.objects.get(patient__exact=pat,date__exact=casedate[0],condition__exact=self.conditions[0])
                            existingcase.events.add(cat6tup[0])
                            existingcase.save
                            remove_list.append(cat6tup)
                            #move on to the next cat6tup in the list
                            continue
                    # if the earliest event (of any type) and earliest fever event aren't
                    # on the same date, move to the next patient
                    # as another category may be more applicable for the event
                    if abs((earliest_event[1]-cat6tup[1]).days) > 0:
                        break
                    elif (((lastcasedt and abs((cat6tup[1]-lastcasedt).days) > self.recurrence_interval)) or not lastcasedt):
                        case_date = min([cat6tup[1]])
                        try:
                            newcase_dict[pat].append(case_date)
                        except KeyError:
                            newcase_dict[pat] = [case_date]
                        event = cat6tup
                        new_case = Case(patient_id=pat,
                                        provider_id=event[2],
                                        date=case_date,
                                        condition=self.conditions[0],
                                        criteria= 'Category 6',
                                        source=self.uri,
                                        status=self.status )
                        new_case.save()
                        counter += 1
                        new_case.events.add(event[0])
                        new_case.save()
                        lastcasedt=case_date
                        remove_list.append(cat6tup)
                    elif lastcasedt and abs((cat6tup[1]-lastcasedt).days) <= self.recurrence_interval:
                        #if there is a lastcasedt and the current event is within the recurrence_interval window,
                        #  we can assume new_case was created and we can add more events to it.
                        new_case.events.add(cat6tup[0])
                        new_case.save()
                        remove_list.append(cat6tup)
            except KeyError:
            #we'd get here if there are not cat6 events for this patient, so just break out of the loop for this patient
                break
            cat6_dict[pat]=list(set(cat6_dict[pat])-set(remove_list))



        log.info('Generated %s new cases of covid19' % counter) 
        return counter # Count of new cases
            
#-------------------------------------------------------------------------------
#
# Packaging
#
#-------------------------------------------------------------------------------

covid19_definition = covid19()

def event_heuristics():
    return covid19_definition.event_heuristics

def disease_definitions():
    return [covid19_definition]

