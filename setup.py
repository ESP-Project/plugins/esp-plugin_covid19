'''
                                  ESP Health
                          COVID-19 Disease Definition
                             Packaging Information
                                  
@author: Bob Zambarano <bzambarano@commoninf.com>
@organization: Commonwealth Informatics.
@contact: http://esphealth.org
@copyright: (c) 2020 Commonwealth Informatics
@license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
'''

from setuptools import setup
from setuptools import find_packages

setup(
    name = 'esp-plugin-covid19',
    version = '1.7',
    author = 'Bob Zambarano',
    author_email = 'bzambarano@commoninf.com',
    description = 'COVID-19 syndromic definition module for ESP Health application',
    license = 'LGPLv3',
    keywords = 'COVID-19 algorithm disease surveillance public health epidemiology',
    url = 'http://esphealth.org',
    packages = find_packages(exclude=['ez_setup']),
    install_requires = [
        ],
    entry_points = '''
        [esphealth]
        disease_definitions = covid19:disease_definitions
        event_heuristics = covid19:event_heuristics
    '''
    )
